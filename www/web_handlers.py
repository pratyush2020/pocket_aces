import json


class RequestException(Exception):
    def __init__(self, status_code, message, data):
        self.status_code = status_code
        self.message = message
        self.data = data

def call_method(calling_method, *args, **kwargs):
    try:
        data = calling_method(args, kwargs)
        return {'data' : data, 'status_code' : 200, 'message' : 'Success'}
    except RequestException as e:
        return json.dumps(e)
    except Exception as e:
        return {'data' : {}, "status_code" : 500, "message" : e}
