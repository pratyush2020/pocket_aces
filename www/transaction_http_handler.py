from www.web_handlers import call_method, RequestException
from lib.globals import *
import cherrypy


class Transaction(object):

    def get_transaction(self, *args, **kwargs):
        if len(args) == 0:
            return RequestException(400, "transaction id not found in request", {})
        return TransactionService.get_transaction(args[0])

    def get_transaction_type_list(self, *args, **kwargs):
        if len(args) == 0:
            return RequestException(400, "transaction type not found in request", {})
        return TransactionService.get_transaction_by_type(args[0])

    def upsert_transaction(self, *args, **kwargs):
        if kwargs.get('amount') is None or kwargs.get('type') is None or kwargs.get('parent_id') is None or len(args) == 0:
            return RequestException(400, "request missing amount/type/parent_id/transaction_id", {})
        TransactionService.upsert_transaction(args[0], kwargs['amount'], kwargs['parent_id'], kwargs['type'])
        return {}

    def get_transaction_sum(self, *args, **kwargs):
        if len(args) == 0:
            return RequestException(400, "transaction id not found in request", {})
        transaction_amount  = TransactionService.get_transaction(args[0])
        if transaction_amount is None:
            return RequestException(204, "Transaction not found", {})
        return transaction_amount


#Url Mapper
class TransactionWrapper(object):
    def __init__(self):
        self.transaction = Transaction()

    @cherrypy.expose
    def get(self, *args, **kwargs):
        return call_method(self.transaction.get_transaction, args, kwargs)

    @cherrypy.expose
    def types(self, *args, **kwargs):
        return call_method(self.transaction.get_transaction_type_list, args, kwargs)

    @cherrypy.expose
    def put_transaction(self, *args, **kwargs):
        return call_method(self.transaction.upsert_transaction, args, kwargs)

    @cherrypy.expose
    def sum(self, *args, **kwargs):
        return call_method(self.transaction.get_transaction_sum, args, kwargs)
