import cherrypy
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base

from www.transaction_http_handler import TransactionWrapper
from service.transaction_service import TransactionService
from lib import globals

if __name__ == '__main__':
    globals.TransactionService = TransactionService()
    globals.SQLAlchemyEngine = create_engine('mysql:////tmp/teste.db', echo=True)
    globals.SQLAlchemyBase= declarative_base(bind=globals.SQLAlchemyEngine)

    cherrypy.config.update({'server.socket_port': 9090})
    cherrypy.tree.mount(TransactionWrapper(), '/transactionservice', None)
    cherrypy.quickstart()
