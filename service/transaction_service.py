from database_lib import transaction as transaction_lib


class TransactionService():

    def get_transaction(self, transaction_id):
        return transaction_lib.get_transaction_by_id(transaction_id)

    def get_child_transactions_sum(self, transaction_id):
        transaction = transaction_lib.get_transaction_by_id(transaction_id)
        if transaction is None:
            return 0
        return transaction.total_sum

    def get_transaction_by_type(self, transaction_type):
        return transaction_lib.get_transaction_by_type(transaction_type)

    def upsert_transaction(self, transaction_id, amount, parent_id, transaction_type):
        transaction = transaction_lib.get_transaction_by_id(transaction_id)
        total_sum = amount
        if transaction is not None:
            total_sum = transaction.total_sum + amount - transaction.amount
        return transaction_lib.upsert_transaction(transaction_id, amount, parent_id, transaction_type, total_sum)
