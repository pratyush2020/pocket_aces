from lib.globals import *
from database_model.transaction import Transaction

def get_transaction_by_id(transaction_id):
    session = SQLAlchemySession()
    return session.query(Transaction).filter(Transaction.id == transaction_id).one()

def get_transaction_by_type(transaction_type):
    session = SQLAlchemySession()
    return session.query(Transaction).filter(Transaction.type == transaction_type).all()

def upsert_transaction(transaction_id, amount, parent_id, transaction_type, total_sum):
    transaction = Transaction(
            parent_id = parent_id,
            id = transaction_id,
            amount = amount,
            transaction_type = transaction_type,
        total_sum = total_sum
    )
    session = SQLAlchemySession()
    session.merge(transaction)
    session.commit()
    #Run the procedure to update all the parents with the sum