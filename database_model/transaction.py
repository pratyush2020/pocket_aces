from sqlalchemy import Column, Integer, Unicode, UnicodeText, String, FLOAT, ForeignKey,DECIMAL
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from random import choice
from string import letters
from lib.globals import SQLAlchemyBase

class Transaction(SQLAlchemyBase):
    __tablename__ = 'transactions'
    id = Column(Integer, primary_key=True)
    amount= Column(FLOAT, nullable=False)
    type = Column(String, nullable=False)
    total_sum = Column(FLOAT, nullable=False)
    parent_id = Column(Integer, ForeignKey(id))